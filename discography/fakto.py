def duz_faktoriyel(girdi):
    sonuc = 1
    for i in range(1,girdi+1):
        sonuc *=  i
    return sonuc

def rekursif_faktoriyel(girdi):
    if girdi < 1:
        return 1
    else:
        sonuc = girdi * rekursif_faktoriyel (girdi - 1)
        return sonuc


def fibonacci( pos ):
        if pos <= 1 :
                return 0
        if pos == 2:
                return 1
        n_1 = fibonacci( pos-1 )
        n_2 = fibonacci( pos-2 )
        n = n_1 + n_2
        return n





print(f"""Özyinelemli sonuc {rekursif_faktoriyel(3)}
Düz sonuc {duz_faktoriyel(3)}""")