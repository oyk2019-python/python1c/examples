argumanlar = ["yeter", "artık", "yetheeer"]
anahtarlı_argumanlar = {"tarhana":True, "cacik":False}

def neler_oluyor_ya(*args, **kwargs):
    print(args)
    print(kwargs)
    
neler_oluyor_ya(*argumanlar, **anahtarlı_argumanlar)
neler_oluyor_ya(argumanlar[0], argumanlar[1], argumanlar[2],
                tarhana = anahtarlı_argumanlar.get("tarhana"),
                cacik = anahtarlı_argumanlar.get("cacik"))