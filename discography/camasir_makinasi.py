def camasir_makinası(derece, kirlilik, sıkma=True, yıkama=True):
    if kirlilik == "az":
        if derece >= 30 and yıkama == True:
            return 1
        else:
            return 0
    elif kirlilik == "cok":
        if derece > 40 and yıkama == True and sıkma == True:
            return 1
        else:
            return 0
    elif kirlilik == "orta":
        if derece > 35 and yıkama == True and sıkma == True:
            return 1
        else:
            return 0

girdiler = [36,"orta"]
cikti = camasir_makinası(*girdiler, False)
if cikti == 1:
    print("Tebrikler camaşırlarınız mis gibi oldu")
else:
    print("Puh bi çamaşır yıkayamadın be")