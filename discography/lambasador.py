hesap_makinasi ={"topla": lambda x,y:x+y,
                 "carp": lambda x,y:x*y,
                 "bol": lambda x,y:x/y,
                 "cikar": lambda x,y:x-y}

print(f"Tatlı lambda {hesap_makinasi.get('topla')(3,5)}")

hp = lambda x,y,z : {"topla" : x+y, "cikar":x-y, "bol":x/y, "carp":x*y}[z]

print(f"cirkin lambda {hp(3,5,'topla')}")