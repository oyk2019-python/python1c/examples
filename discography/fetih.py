liste = ["1a2bcf", "-1453", "1453", "ad,hoc,zulu,lima,gama,alfa", "13.72"]

for elem in liste:
    if elem.startswith("-"):
        if elem[1:].isdecimal():
            cikti = bin(int(elem))
            print(cikti[::-1])
    else:
        if elem.isdecimal():
            cikti = bin(int(elem))
            print(cikti[::-1])
