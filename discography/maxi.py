def karsilik_bul(*args, **kwargs):
    print(args, kwargs)

sozluk = {"kitap"      : "book",
          "bilgisayar" : "computer",
          "programlama": "programming"}

karsilik_bul("kitap", "bilgisayar", "programlama", \
             "fonksiyon", **sozluk)
karsilik_bul("kitap", "bilgisayar", "programlama", \
             "fonksiyon", *sozluk)