from kitap import Kitap
from insan import Insan

kitap1 = Kitap(
    adi="Beyaz Geceler",
    dil="Türkçe",
    yazar="Dostoyevski"
)
kitap2 = Kitap(
    adi="Sefiller",
    dil="İngilizce",
    yazar="Hugo",
    kiralanabilir=False,
    satilabilir=True
)
kitap3 = Kitap(
    adi="Savaş ve Barış",
    dil="Türkçe",
    kiralanabilir=False,
    yazar="Tolstoy"
)

insan1 = Insan("Hamdi")
insan2 = Insan("Fatma")

insan1.kitap_kirala(kitap1)
insan1.kitap_satin_al(kitap2)
insan1.kitap_oku(kitap1)
insan1.kitap_oku(kitap2)

insan1.okudugum_kitaplari_listele()
