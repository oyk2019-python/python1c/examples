from uuid import uuid1

class Kitap():
    def __init__(self, **kwargs):
        self.uuid = uuid1()
        self.adi = kwargs["adi"]
        self.dil = kwargs["dil"]
        self.yazar = kwargs["yazar"]
        kiralanabilir = kwargs.get("kiralanabilir")
        if kiralanabilir is None:
            self.kiralanabilir = True
        else:
            self.kiralanabilir = kiralanabilir
        satilabilir = kwargs.get("satilabilir")
        if satilabilir is None:
            self.satilabilir = False
        else:
            self.satilabilir = satilabilir
        
    
    def __str__(self):
        return f"<{self.uuid} '{self.adi}'>"


class Magaza():
    def __init__(self, adi):
        self.kitaplar = {}
        self.adi = adi
    
    def kitap_al(self):
        # mağaza'ya kitap ekle
        pass
    
    def kitap_sat(self, insan, kitap):
        # mağazadan bir insana kitap sat
        pass
    
    def kitap_kirala(self):
        # mağazadan bir insana kitap kirala
        pass
    
    def kitaplari_listele(self):
        # kitapları listele
        pass
    
    
    








