def kare(x):
    return x**2

el_kare = lambda x: x**2

def yaz(a, b):
    print(a, b)

el_yaz = lambda a, b: print(a, b)

el_yaz("string", 9)
el_kare(9)

k = kare(4)
y = yaz(5, 6)

print("k: ", k, "y: ", y)
