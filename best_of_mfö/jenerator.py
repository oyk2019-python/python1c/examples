def f():
    for i in range(100, 1, -1):
        yield i**2

def g():
    dondur = []
    for i in range(100, 1, -1):
        dondur.append(i**2)
    return dondur

for i in f():
    if i == 2500:
        print("bitti")

#a = {x**2 for x in [2, 2]}
#print(a)
isim = "çağrı"
print(isim, set(isim))
turkish_char = set("çÇşŞıİöÖüÜğĞ")
print(turkish_char)
print(turkish_char.intersection(set(isim)))