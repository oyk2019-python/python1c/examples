stok = {
    "elma": 10
}

fiyatlar = {
    "elma": {
        "alış": 1,
        "satış": 2
    }
}

kar_zarar = {
    "elma": 4
}

def stok_yazdir():
    print(f"Stok durumu: {stok}")

def stoga_urun_ekle(adi, adet, alis, satis):
    if stok.get(adi):
        print(f"{adi} ürününden {stok[adi]} adet var.")
        stok[adi] += int(adet)
        # stok[adi]['adet'] = stok[adi]['adet'] + int(adet)
        fiyatlar[adi]['alis'] = float(alis)
        fiyatlar[adi]['satis'] = float(satis)
    else:
        stok[adi] = int(adet)
        fiyatlar[adi] = {
            "alış": float(alis),
            "satış": float(satis)
        }
        kar_zarar[adi] = 0
    print(f"{adi} stoğu güncellendi. Güncel adet: {stok[adi]}")

def satis_yap(adi, adet):
    if stok.get(adi, 0) >= int(adet):
        stok[adi] -= int(adet)
        kar = (fiyatlar[adi]["satış"] - fiyatlar[adi]["alış"]) * int(adet)
        kar_zarar[adi] += kar
    else:
        print(f"Stokta {stok[adi]} adet {adi} var!")

def toplam_kar_hesapla():
    toplam_kar = sum(kar_zarar.values())
    print(f"Toplam kar: {toplam_kar}")

def urun_bazli_kar_hesapla(adi):
    urun_bazli_kar = kar_zarar[adi]
    print(f"{adi} ürününden edilmiş toplam kar: {urun_bazli_kar}")

def gun_sonu():
    stok_yazdir()
    toplam_kar_hesapla()

while True:
    print("""
    0. Stoklara yeni ürün girişi
    1. Ürün satışı
    2. Karlılık hesapla
    3. Ürün bazlı karlılık hesapla
    4. Gün sonu""")
    secenek = input("Yapmak isteğiniz eylemi girin: ")
    secenek = int(secenek)
    if secenek == 0:
        urun_adi = input("Ürün adını girin: ")
        urun_adeti = input("Ürün adetini girin: ")
        urun_alis = input("Ürün alış fiyatını girin: ")
        urun_satis = input("Ürün satış fiyatını girin: ")
        stoga_urun_ekle(urun_adi, urun_adeti,
                        urun_alis, urun_satis)
    elif secenek == 1:
        stok_yazdir()
        urun_adi = input("Ürün adını girin: ")
        urun_adeti = input("Ürün adetini girin: ")
        satis_yap(urun_adi, urun_adeti)
    elif secenek == 2:
        toplam_kar_hesapla()
    elif secenek == 3:
        urun_adi = input("Ürün adını girin: ")
        urun_bazli_kar_hesapla(urun_adi)
    elif secenek == 4:
        gun_sonu()
        break
    else:
        print(f"lütfen tanımlı bir secenek giriniz.")
